from django.urls import path
from.views import registerUser, home

app_name = "startpage"


urlpatterns = [
    path('', home, name="home"),
]