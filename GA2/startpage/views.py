from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def registerUser(request):
    if request.method == "POST":
        print(request.POST)
    else:
        form = UserCreationForm()
    return render(request, 'startpage/registerUser.html', {'form':form})

def home(request):
    return render(request, 'startpage/home.html')